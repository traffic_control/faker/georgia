<?php

declare(strict_types=1);

namespace Faker\Georgia;

use Faker\Generator;

class Factory extends \Faker\Factory
{
    public static function georgia(): Generator
    {
        $generator = new Generator();

        foreach (static::$defaultProviders as $provider) {
            $providerClassName = static::getProviderClassname($provider);
            $generator->addProvider(new $providerClassName($generator));
        }

        return $generator;
    }

    /**
     * {@inheritDoc}
     */
    protected static function getProviderClassname($provider, $locale = 'ka_GE')
    {
        $providerClass = 'Faker\\Georgia\\' . $provider;

        if (class_exists($providerClass, true)) {
            return $providerClass;
        }

        return parent::getProviderClassname($provider, $locale);
    }
}

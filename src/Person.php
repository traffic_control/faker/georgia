<?php

namespace Faker\Georgia;

use Faker\Extension\Extension;

class Person extends \Faker\Provider\ka_GE\Person implements Extension
{
    const ID_OLD = 'OLD';
    const ID_NEW = 'NEW';

    protected $georgianLetters = [
        'ა', 'ბ', 'გ', 'დ', 'ე', 'ვ', 'ზ', 'თ', 'ი', 'კ', 'ლ', 'მ', 'ნ', 'ო', 'პ', 'ჟ', 'რ',
        'ს', 'ტ', 'უ', 'ფ', 'ქ', 'ღ', 'ყ', 'შ', 'ჩ', 'ც', 'ძ', 'წ', 'ჭ', 'ხ', 'ჯ', 'ჰ'
    ];

    /**
     * @link http://www.refworld.org/docid/3ae6accf24.html
     */
    public function personalNo()
    {
        return static::numerify('###########');
    }

    public function idNumber($type = null)
    {
        if (!$type) {
            $type = static::randomElement([static::ID_NEW, static::ID_OLD]);
        }
        if ($type == static::ID_NEW) {
            return strtoupper(static::bothify('##??#####'));
        }

        return static::randomElement($this->georgianLetters) . static::numerify('#######');
    }
}

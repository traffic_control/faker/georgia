<?php

namespace Faker\Test\Georgia;

use Faker\Georgia\Factory;
use Faker\Georgia\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    public function testPersonalNo()
    {
        $faker = Factory::georgia();
        $this->assertEquals(11, strlen($faker->personalNo()));
    }

    public function testIdNumber()
    {
        $faker = Factory::georgia();
        $this->assertEquals(9, strlen($faker->idNumber(Person::ID_NEW)));
        $this->assertEquals(10, strlen($faker->idNumber(Person::ID_OLD)));
    }
}
